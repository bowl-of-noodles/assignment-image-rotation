#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "../include/image.h"

struct image transform(const struct image *image);

#endif
