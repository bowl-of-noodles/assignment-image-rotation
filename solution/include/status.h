#ifndef STATUS_H
#define STATUS_H

enum status {
    SUCCESS = 0,
    OPEN_ERROR,
    CLOSE_ERROR,
    READ_ERROR,
    WRITE_ERROR,
};

#endif

