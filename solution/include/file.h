#ifndef FILE_H
#define FILE_H

#include <status.h>
#include <stdio.h>


enum status file_open(const char *file_name, FILE **file, char *mode);
enum status file_close(FILE *file);

#endif
