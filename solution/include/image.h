#ifndef IMAGE_H
#define IMAGE_H

#include <stddef.h>
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width, height;
    struct pixel* data;
};

struct image image_create(size_t width, size_t height);

void image_destroy(struct image image);

#endif

