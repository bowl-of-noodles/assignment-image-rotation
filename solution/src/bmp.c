#include "../include/bmp.h"
#include "stdint.h"
#include <stdbool.h>


bool read_header(FILE *file, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, file);
}

size_t size_of_file(const size_t image_size) {
    return image_size + sizeof(struct bmp_header);
}

size_t get_padding_size(size_t width){
    return width % 4 == 0 ? 0 : (width*3/4 + 1)* 4 - width*3;
}

size_t size_of_image(const struct image *image) {
    return image->height * (3 * image->width + get_padding_size(image->width));
}

enum status from_bmp(FILE *in, struct image *image) {
    struct bmp_header header = {0};
    if (!read_header(in, &header)) {
        return READ_ERROR;
    }

    *image = image_create(header.biWidth, header.biHeight);

    size_t i = 0;
    size_t j = 0;
    
    while (i < image->height){
    	while (j < image->width){
    		fread(&(image->data[image->width * i + j]), sizeof(struct pixel), 1, in);
    		j++;
    	}
    	fseek(in, (long) get_padding_size(image->width), SEEK_CUR);//указатель с текущей позиции
    	j=0; //чтобы начать с 0 
        i++; //позиции новой "высоты"
    }
    
    return SUCCESS;
}

enum status to_bmp(FILE *out, const struct image *image) {

    struct bmp_header header = {0};
   
    header.bfType = 19778;
    header.bfileSize = size_of_file(size_of_image(image));
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = size_of_image(image);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
	
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    fseek(out, header.bOffBits, SEEK_SET);

    const uint8_t zero = 0;
    
    size_t i = 0;
    size_t j = 0;
    
    if (image->data) {
        while (i < image->height){
        	fwrite(image->data + i * image->width, image->width * sizeof(struct pixel), 1, out);
        	while (j < get_padding_size(image->width)){
        		fwrite(&zero, 1, 1, out);
        		j++;
        	}
       	i++;
        j=0;
        }      
    } else {
        return WRITE_ERROR;
    }

    return SUCCESS;
}
