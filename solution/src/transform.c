#include "../include/transform.h"
#include <malloc.h>


struct image transform(const struct image *image) {

    if (!image->data) {
        return (struct image) {.width = image->width, .height = image->height, .data = NULL};
    }

    struct pixel *data = malloc(sizeof(struct pixel) * image->width * image->height);

    
    for (size_t i = 0; i < image->height; ++i) {
        for (size_t j = 0; j < image->width; ++j) {
            data[image->height * j + (image->height - i - 1)] = image->data[i * image->width + j];
        }
    }

    return (struct image) {.width = image->height, .height = image->width, .data = data};
      
}
