#include "../include/image.h"
#include <malloc.h>

struct image image_create(const size_t width, const size_t height) {
 return ((struct image) {
    	   .width = width, 
    	   .height = height,
           .data = malloc(width * height * sizeof(struct pixel))});

}


void image_destroy(struct image image) {
  free(image.data);
}
