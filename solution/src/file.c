#include "file.h"

/*enum status file_open(const char *pathname, FILE *file, const char *mode) {
  if ((file = fopen(pathname, mode)) == NULL)
    return OPEN_ERROR;
  else
    return SUCCESS;
}*/

enum status file_open(const char *file_name, FILE **file, char *mode) {

    *file = fopen(file_name, mode);
    if (*file == NULL) {
        return OPEN_ERROR;
    }

    return SUCCESS;
}


enum status file_close(FILE *file) {
  if (fclose(file) == EOF)
    return CLOSE_ERROR;
  else
    return SUCCESS;
}
